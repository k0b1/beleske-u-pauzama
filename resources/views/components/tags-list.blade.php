<?php
$tags_array = json_decode($post->post_tags, true);
?>
<p class="zenit-tags__row">
    <span class="text-xs">Tagovi:</span>
    @if (!empty($tags_array))
    @foreach ($tags as $tag)
    @foreach ($tags_array as $selected_tag)
    @if ($selected_tag == $tag->id)
    <a href="/tag/{{$tag->id}}" class="text-black zenit-color__purple-hover hover:no-underline">#{{$tag->tag_name}}</a>
    @endif
    @endforeach
    @endforeach
    @endif
</p>