<?php
// $post_limit = $limit;
$number_of_pages = ceil($number_of_posts / $limit);
// echo $number_of_posts . " pp " . $limit;
?>

<div class="pagination-container">
    @if ($number_of_pages > 1)
    @foreach(range(1, $number_of_pages) as $pagination_item)
    <a href="?page={{$pagination_item}}" class="pagination-item">{{ $pagination_item }}</a>
    @endforeach
    @endif
</div>