<div class="mt-3">
    <p>Tags:</p>
    <p>
        <input id="dashboard-selected-tags" class="border bg-white" value="{{$selectedTags}}" placeholder="select tags" />
    </p>
    <input id="filter-field" name="filter-field" type="text" value="" style="display: none" />
    <select id="all-tags-hidden" name="noname" style="display:none">
        @foreach ($data as $tag)
        <option value="{{$tag['id']}}">{{$tag['tag_name']}}</option>
        @endforeach
    </select>
    <select id="dashboard-multiple-tags" name="dashboard-multiple-tags[]" multiple="multiple" class="selectpicker w-100" style="display:none">
        @foreach ( (array)json_decode($selectedTags) as $tag)
        <option class="tag-option" value="{{$tag}}" selected> $tag</option>
        @endforeach


    </select>
</div>
<script>
    const selectedTags = document.getElementById('dashboard-selected-tags');
    let multipleSelect = document.getElementById('dashboard-multiple-tags');
    const hiddenTags = document.getElementById('all-tags-hidden');
    const filterField = document.getElementById('filter-field')
    let typedTag = "";
    let resultArray = {};
    let defaultTagsSelect;

    // test
    let clickedOptions = document.getElementById('dashboard-multiple-tags')
    console.log("CLICKED", clickedOptions)
    // clickedOptions.addEventListener('click', (event) => {
    //     console.log("EVENT", event)
    // })
    // end of test


    const createObjectFromSelectOptions = (selectObj) => {
        const obj = {};
        for (var i = 0; i < selectObj.options.length; i++) {
            obj[selectObj.options[i].value] = selectObj.options[i].innerHTML
        }
        return obj;
    }

    const tagsObject = createObjectFromSelectOptions(hiddenTags);

    const createSelectOptionsFromObject = (obj, selectEl) => {
        for (tag in obj) {
            var opt = document.createElement('option');
            opt.value = tag;
            opt.innerHTML = obj[tag];
            selectEl.appendChild(opt);
        }
        return selectEl;
    }

    var updateSelectOptionsFor = function(selectObj) {
        let selectParentNode = selectObj.parentNode;
        let newSelectObj = selectObj.cloneNode(false); // Make a shallow copy
        selectParentNode.replaceChild(newSelectObj, selectObj);
        return newSelectObj;
    }

    // show filted field and all available options
    selectedTags.addEventListener('click', () => {
        filterField.style.display = filterField.style.display == "none" ? "block" : "none";
        multipleSelect.style.display = multipleSelect.style.display == "none" ? "block" : "none";

        defaultTagsSelect = createSelectOptionsFromObject(tagsObject, updateSelectOptionsFor(multipleSelect))
        multipleSelect = defaultTagsSelect // release
    })

    // create string from filter input and filter tags based on it
    filterField.addEventListener('keydown', (event) => {
        if ((event.keyCode >= 48 && event.keyCode <= 90) || (event.keyCode >= 97 && event.keyCode <= 122)) {
            typedTag += event.key
            for (tag in tagsObject) {
                if (typedTag == tagsObject[tag].substring(0, 1)) {
                    resultArray[tag] = (tagsObject[tag])
                }
            }
            let filteredTags = createSelectOptionsFromObject(resultArray, updateSelectOptionsFor(multipleSelect))
            multipleSelect = filteredTags
        }
        if (event.keyCode == 8) {
            typedTag = "";
            resultArray = {}
            defaultTagsSelect = createSelectOptionsFromObject(tagsObject, updateSelectOptionsFor(multipleSelect))
            multipleSelect = defaultTagsSelect
        }
    })
</script>