@foreach ($post as $post)
@section('title', $post->post_title)
@section('meta_description', $post->meta_description)
@section('meta_keywords', $post->meta_keywords)
@include('layouts.header')
<div class="sm:block lg:hidden">
    @include('layouts.navigation')
</div>
<div class="container-fluid">
    <div class="row d-flex justify-content-center">
        <div class="col-md-12 bg-white">
            <div class="row">
                <div class="col-md-9">
                    @include('layouts.title-and-subtitle')
                    <div class="row">
                        <div class="col-md-3 col-lg-3">
                            <h2 class="zenit__blog-title zenit-color__purple otoman text-xl">
                                {{$post->post_title}}
                            </h2>
                            <div class="zenit-blog__meta">
                                <!-- <p>Kontakt: {{$post->email}}</p> -->
                                <p class=" text-sm mb-0"> Objavljeno:</p>
                                <p class=" text-sm"> {{ date_format( new DateTime($post->created_at),"d.m.Y / H:i:s") }}</p>
                                @include('components.tags-list')
                            </div>
                            <img alt="" src="{{asset('storage/'.$post->post_image)}}" />
                        </div>
                        <div class="col-md-9 pl-4 mb-16 col-lg-7">
                            @if (!empty($post['post_logo']) )
                            <img alt="Blog logo image" src="{{$post->post_logo}}"" class=" m-auto" />
                            @endif
                            <div class="text-base ">{!! html_entity_decode($post->post_content) !!}</div>
                        </div>
                    </div>
                </div>
                <div class="blog-sidebar-container lg:col-md-3 lg:fixed p-0 right-0 h-100 hidden lg:flex">
                    @include('layouts.sidebar')
                </div>
            </div>
            @endforeach

            <div class="sm:block md:block lg:hidden">
                @include('layouts.footer')
            </div>
