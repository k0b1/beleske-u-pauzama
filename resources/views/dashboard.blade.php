<x-app-layout>
    <div class="md:flex">
        @include('dashboard.dashboard-sidebar')
        <div class="w-full md:w-3/4 m-3 bg-white-400">
            <h4 class="uppercase font-mono">Welcome to Dashboard</h4>
            <p>Edit, delete, update your posts, categories and tags!</p>
        </div>
    </div>

</x-app-layout>
