@include('layouts.header')
<div class="sm:block lg:hidden">
  @include('layouts.navigation')
</div>
<div class="container-fluid">
  <div class="row flex justify-content-center">
    <div class=col-md-12 bg-white">
      <div class="row">
        <div class="col-md-9 border-right-secondary-color">
          @include('layouts.title-and-subtitle')
          <h2 class="uppercase zenit__blog-page-title pt-2 inline-block border-black text-xl mb-20 mt-10">
            Blog
          </h2>
          <div class="blog-content-list justify-center d-flex flex-wrap mb-4">
            @foreach ($posts as $post)

            <div class="col-md-9 md:mb-5 ml-2 mt-4 sm:mb-1">
              <h6 class="blog-content-list__title">
                <a class="uppercase text-3xl no-underline" href="/post/{{$post->post_slug}}">
                  <span class="blog-content-list__pre-title pr-2">@-></span>{{$post->post_title}}
                </a>
              </h6>

              <div class="row justify-center">
                <div class="col-md-10">
                  <p class="text-sm italic text-black">
                    {{$post->excerpt}}
                  </p>
                  <div class="mb-8 d-flex">
                    <p class="text-xs pr-1">Vreme objave: </p>
                    <p class="text-xs pr-1 mb-0 mt-0 text-black">{{date('l,', strtotime($post->created_at))}}</p>
                    <p class="text-xs pr-1 mb-0 mt-0 text-black">{{date('jS \of F Y,', strtotime($post->created_at))}}</p>
                    <p class="text-xs pr-1 text-black">{{date('H:i:s', strtotime($post->created_at))}}</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 text-5xl d-flex justify-center amatic">###</div>
            </div>
            @endforeach
          </div>
          @include('components.pagination')
        </div>
        <div class="blog-sidebar-container lg:col-md-3 lg:fixed p-0 right-0 h-100 hidden lg:flex">
          @include('layouts.sidebar')
        </div>
      </div>
    </div>
  </div>
</div>
<div class="sm:block md:block lg:hidden">
  @include('layouts.footer')
</div>