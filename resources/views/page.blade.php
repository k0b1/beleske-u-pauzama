@foreach ($page as $post)
@section('title', $post->post_title)
@section('meta_description', $post->meta_description)
@section('meta_keywords', $post->meta_keywords)
@include('layouts.header')
<div class="container-fluid">
    <div class="row d-flex justify-content-center">
        <div class="col-md-12 bg-white">
            <div class="row">
                <div class="col-md-9">
                    @include('layouts.title-and-subtitle')
                    <div class="row">
                        <div class="col-md-2 col-lg-2">
                        </div>
                        <div class="col-md-10 pl-4 col-lg-7">
                            <h2 class="zenit__blog-title zenit-color__purple otoman text-xl">
                                {{$post->title}}
                            </h2>
                            @if (!empty($post['post_logo']) )
                            <img alt="Blog logo image" src="{{$post->post_logo}}"" class=" m-auto" />
                            @endif
                            <div class="text-base ">{!! html_entity_decode($post->content) !!}</div>
                        </div>
                    </div>
                </div>
                <div class="blog-sidebar-container lg:col-md-3 lg:fixed p-0 right-0 h-100 hidden lg:flex">
                    @include('layouts.sidebar')
                </div>
            </div>
            @endforeach
