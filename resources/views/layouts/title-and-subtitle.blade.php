<nav class="blog-header navbar-light d-flex items-center mb-32">
    <div class="blog-navigation">
	<span class="blog-navigation__title">Sekcije: </span>
	<ul class="blog-navigation__list">
	    <li class="blog-navigation__links"><a href="{{URL::to('/blog')}}" title="Link ka blog sekciji">blog</a></li>
	    <li class="blog-navigation__links"><a href="{{URL::to('/bitovi-i-bajtovi')}}" title="Link ka kompjuterskoj sekciji">0&1</a></li>
	    <li class="blog-navigation__links"><a href="{{URL::to('/osvrti')}}" title="Link ka recenzijama">osvrti</a></li>
	    <li class="blog-navigation__links"><a href="{{URL::to('/page/about')}}" title="Link ka O meni sekciji">ich</a></li>
	</ul>
    </div>
    <h1 class="ms-5 mb-4 relative logo">
        <a href="/" class="zenit-home__link secondary-color" title="Zenit blog title">
            Povezivanje stvari
        </a>
        <span class="left-0 text-black logo__subtext">
            <span class="secondary-color">#</span>00101010
	    <span class="secondary-color">#</span>bagovi
	    <span class="secondary-color">#</span>stvarnost
	    <span class="secondary-color">#</span>uradi sam
        </span>
    </h1>
    <img alt="wavy line" src="{{ URL::asset('images/wave.png') }}" class="blog-resources__wavy-line" />
</nav>
