<!DOCTYPE html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <title>@yield('title','Povezivanje stvari')</title>
  <meta name="description" content="@yield('meta_description', 'Bez jasnog pravila, različite teme vezane za tehnologiju, politiku, psihologiju, programiranje, ljudsku svest, kontrakulturu.')" />
  <meta name="keywords" content="@yield('meta_keywords','cyberpunk, programiranje, kognitivna psihologija, inteligencija, javascript, internet')">

  <link rel="canonical" href="{{url()->current()}}" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <link rel="apple-touch-icon" href="/apple-touch-icon.png" />

  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="{{ URL::asset('css/app.css') }}" rel="stylesheet" />
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Place favicon.ico in the root directory -->
  <!-- Latest compiled and minified CSS -->

</head>

<body class="font-sans antialiased min-h-full">
  @include('layouts.admin-navigation')

  <!--[if lt IE 8]>
          <p class="browserupgrade">
          You are using an <strong>outdated</strong> browser. Please
          <a href="http://browsehappy.com/">upgrade your browser</a> to improve
          your experience.
          </p>
      <![endif]-->
