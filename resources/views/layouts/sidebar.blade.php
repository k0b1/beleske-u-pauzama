@include('layouts.navigation')
<div class="sidebar-images__container">
    <img alt="" src="{{ URL::asset('images/sidebar.png') }}" class="sidebar-images__background" />
    <img alt="" src="{{ URL::asset('images/eris.jpg') }}" class="blog-sidebar-image" />
    
</div>
<div class="absolute bottom-0 left-0 right-0">
    @include('layouts.footer')
</div>
