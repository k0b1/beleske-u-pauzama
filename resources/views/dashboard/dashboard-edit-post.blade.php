<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="md:flex">
        @include('dashboard.dashboard-sidebar')
        <div>
            Edit your post if you like it:
            <?php
            if (isset($success)) {
                echo $success;
            }
            ?>

            @include('dashboard.dashboard-post-form',
            array(
            'post'=> $post,
            'author'=>$author,
            'tags' => $tags,
            'categories' => $categories,
            'id' => $id)
            )
        </div>
</x-app-layout>