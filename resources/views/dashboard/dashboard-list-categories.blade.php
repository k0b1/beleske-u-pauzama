<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="md:flex">
        @include('dashboard.dashboard-sidebar')
        <div>
            Here we will list all the posts
            @foreach ($categories as $category)
            <div class="flex mb-4">
                <x-form method="POST" action="/update-category/{{$category->id}}">
                    @csrf
                    <div class="flex">
                        <div class="flex flex-column">
                            <x-input name="category-name" :value="$category->category_name" />
                            <textarea name="category-description">
                            {{$category->category_description}}
                            </textarea>
                        </div>
                        <button class="btn btn-success">Update</button>
                    </div>

                </x-form>

                <x-form method="DELETE" action="/delete-tag/{{$category->id}}">
                    @csrf
                    <button class="btn btn-warning">Delete</button>
                </x-form>
            </div>
            @endforeach
        </div>


    </div>
</x-app-layout>
