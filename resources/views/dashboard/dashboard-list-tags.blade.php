<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="md:flex">
        @include('dashboard.dashboard-sidebar')
        <div>
            <x-form method="POST" action="/create-new-tag">
                @csrf
                <div class="flex">
                    <div class="flex flex-column">
                        <x-input name="tag-name" placeholder="Unesi ime taga" value=""/>
                        <textarea name="tag-description">

                        </textarea>
                    </div>
                    <button class="btn btn-success">Update</button>
                </div>
            </x-form>

            Here we will list all the tags
            @foreach ($tags as $tag)
            <div class="flex mb-4">
                <x-form method="POST" action="/update-tag/{{$tag->id}}">
                    @csrf
                    <div class="flex">
                        <div class="flex flex-column">
                            <x-input name="tag-name" :value="$tag->tag_name" />
                            <textarea name="tag-description">
                            {{$tag->tag_description}}
                            </textarea>
                        </div>
                        <button class="btn btn-success">Update</button>
                    </div>

                </x-form>

                <x-form method="DELETE" action="/delete-tag/{{$tag->id}}">
                    @csrf
                    <button class="btn btn-warning">Delete</button>
                </x-form>
            </div>
            @endforeach
        </div>

    </div>
</x-app-layout>
