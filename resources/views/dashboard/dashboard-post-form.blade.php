@if (!empty($post))
<x-form class="flex justify-end sticky right-0" method="DELETE" action="/delete-post/{{$id}}">
    <button class="btn btn-warning">Delete</button>
</x-form>
<x-form method="POST" class="mb-5" enctype="multipart/form-data" action="/dashboard-edit-post/{{$id}}">
    @csrf
    <div class="md:flex flex-column p-3">
        {!! print_r($categories) !!}
        @foreach ($post as $post_data)
        <label class="mt-4">Title</label>
        <x-input name="post-title" class="form-control" value="{{$post_data->post_title}}" />
        <label class="mt-4">Slug</label>
        <x-input name="post-slug" class="form-control" value="{{$post_data->post_slug}}" />
        <label class="mt-4">Podnaslov</label>
        <x-input name="post-subtitle" class="form-control" value="{{$post_data->post_subtitle}}" />
        <div class="mt-6">
            <label for="image-upload">Image Upload</label>
            <p>
                <input id="post-image" class="btn btn-outline-secondary" type="file" name="file" accept="image/png, image/jpeg" />
                @if (!empty($post_data->post_image) )
                <p>Trenutno izabrana slika posta: </p>
                <img alt="" src="{{asset('storage/'.$post_data->post_image)}}" />
                @endif
            </p>
        </div>
        <label class="mt-4">Sazetak</label>
        <x-input name="post-excerpt" class="form-control" value="{{$post_data->excerpt}}" />
        <label class="mt-4">Kategorija</label>
        <select name="post-category" class="form-control">
            @foreach ($categories as $category)
            <option value="{{$category['id'] }}">{!! $category['category_name'] !!}</option>
            @endforeach
        </select>
        <label class="mt-4">Author</label>
        <x-input readonly name="" class="form-control" value="{{$author[0]->name}}" />
        <x-input type="hidden" name="post-author" value="{{$author[0]->post_author}}" />
        <label class="mt-4">Date</label>
        <x-input name="post-date" class="form-control" value="{{$post_data->created_at}}" />
        <label class="mt-4">Meta tags</label>
        <x-input name="meta-keywords" class="form-control" value="{{$post_data->meta_keywords}}" placeholder="List of meta tags, separated by ," />
        <label class="mt-4">Meta description</label>
        <x-input name="meta-description" class="form-control" value="{{$post_data->meta_description}}" placeholder="Meta description..." />


        <x-select-multiple :data="$tags ?? ''" :selectedTags="$post_data->post_tags"></x-select-multiple>
        <label class="mt-4"> Sadrzaj</label>
        <div class="form-group">
            <textarea class="ckeditor form-control" name="wysiwyg-editor" cols="50" id="ckeditor" rows="10">
            {!! $post_data->post_content  !!}
            </textarea>
        </div>
        @endforeach

    </div>
    <button class="btn btn-success" name="update" value="published">Publish</button>
    <button class="btn btn-warning" name="draft" value="draft">Save Draft</button>
    <a href="/preview/post/{{$post_data->post_slug}}" class="btn btn-warning">View draft</a>

    <input name="" type="submit" clas="btn btn-warning" value="" />
</x-form>
@else
<x-form method="POST" enctype="multipart/form-data" class="mb-5" action="/dashboard-create-post">
    @csrf
    <div class="md:flex flex-column p-3">
        <label class="mt-4">Title</label>
        <x-input name="post-title" class="form-control" value="" />
        <label class="mt-4">Slug</label>
        <x-input name="post-slug" class="form-control" value="" />
        <label class="mt-4">Podnaslov</label>
        <x-input name="post-subtitle" class="form-control" value="" />
        <label class="mt-4">Sazetak</label>
        <x-input name="post-excerpt" class="form-control" value="" />
        <label class="mt-4">Kategorija</label>
        <select name="post-category" class="form-control">
            @foreach ($categories as $category)
            <option value="{{$category['id'] }}">{!! $category['category_name'] !!}</option>
            @endforeach
        </select>
        <div class="mt-6">
            <label for="image-upload">Image Upload</label>
            <p>
                <input id="post-image" class="btn btn-outline-secondary" type="file" name="file" accept="image/png, image/jpeg" />
                @if (!empty($post_data->post_image) )
                <p>Trenutno izabrana slika posta: </p>
                <img alt="" src="{{asset('storage/'.$post_data->post_image)}}" />
                @endif
            </p>
        </div>
        <label class="mt-4">Author</label>
        <x-input readonly name="" class="form-control" value="{{  json_decode($author)[0]->name}}" />
        <x-input type="hidden" name="post-author" class="form-control" value="{{json_decode($author)[0]->id}}" />
        <label class="mt-4">Date</label>
        <x-input name="post-date" class="form-control" value="" placeholder="Example: 2021-02-13 21:47:17" />
        <label class="mt-4">Meta tags</label>
        <x-input name="meta-keywords" class="form-control" value="" placeholder="List of meta tags, separated by ," />
        <label class="mt-4">Meta description</label>
        <x-input name="meta-description" class="form-control" value="" placeholder="Meta description..." />


        <x-select-multiple :data="$tags ?? ''" :selectedTags="json_encode([])"></x-select-multiple>
        <label class="mt-4"> Unesi tekst</label>
        <div class="form-group">
            <textarea class="ckeditor form-control" name="wysiwyg-editor" cols="50" rows="10">
            </textarea>
        </div>
    </div>
    <button class="btn btn-warning" name="update" value="published">Update</button>
    <button class="btn btn-warnig" name="draft" value="draft">Draft</button>
    <button class="btn btn-success">View draft</button>

    <input name="" type="submit" clas="btn btn-warning" value="" />
</x-form>
@endif
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
    CKEDITOR.replace('ckeditor', {
        filebrowserImageUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form',
    });
</script>