<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="md:flex">
        @include('dashboard.dashboard-sidebar')
        <div>
            <p>
                Here we will list all the posts
            </p>
            <div class="table-auto m-3 p-2">
                <div class="border-bottom border-white">
                    <span class="inline-block w-80">Title</span>
                    <span class="inline-block w-60">Datum</span>
                </div>
                @foreach ($posts as $post)
                <div class="flex my-3 border-bottom border-white p-2">
                    <span class="inline-block w-80">
                        <a href="/dashboard-edit-post/{{$post->id}}">
                            {{$post->post_title}}
                        </a>
                    </span>
                    <span class="inline-block w-60">{{$post->created_at}}</span>
                    <a href="/dashboard-edit-post/{{$post->id}}" class="btn btn-success mr-2">Edit</a>
                    <x-form method="DELETE" action="/delete-post/{{$post->id}}">
                        @csrf
                        <button class="btn btn-danger">Delete</button>
                    </x-form>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</x-app-layout>
