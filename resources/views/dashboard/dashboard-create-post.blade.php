<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="md:flex">
        @include('dashboard.dashboard-sidebar')
        <div>
            Create your post if you like it:

            @include('dashboard.dashboard-post-form',
            array(
            'post'=> "",
            'author'=>$author ? $author: "",
            'tags' => $tags ? $tags: "",
            'categories' => $categories)
            )
        </div>
</x-app-layout>