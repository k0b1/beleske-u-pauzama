<div class="w-full md:w-1/4 p-3 bg-purple-300 min-h-screen">
    <div class="flex flex-col text-black">
        <h4 class="uppercase font-mono">Select option</h4>
        <ul>
            <li class="list-disc ml-3">
                <a class="text-black" href="/dashboard-posts">Posts</a>
            </li>
            <li class="list-disc ml-3">
                <a class=" text-black" href="/dashboard-create-post">Create Post</a>
            </li>
            <li class="list-disc ml-3">
                <a class=" text-black" href="/dashboard-pages">Pages</a>
            </li>
            <li class="list-disc ml-3">
                <a class=" text-black" href="/dashboard-create-page">Create Page</a>
            </li>
            <li class="list-disc ml-3">
                <a class=" text-black" href="/dashboard-categories">Categories</a>
            </li>
            <li class="list-disc ml-3">
                <a class=" text-black" href="/dashboard-tags">Tags</a>
            </li>
            <li class="list-disc ml-3">
                <a class=" text-black" href="/dashboard-account-settings">My account</a>
            </li>
            <li class="list-disc ml-3">
                <a class=" text-black" href="/">Visit web site</a>
            </li>
        </ul>
    </div>
</div>