require("./bootstrap");

require("alpinejs");

/** MATRIX BACKGROUND */
const displayMatrixBackground = (container, canvasEl, decrease) => {
    const getMatrixElement = document.getElementById(container);
    if (getMatrixElement) {
        const getMatrixElementWidth = getMatrixElement.clientWidth;
        const canvas = document.getElementById(canvasEl);

        if (canvas.getContext) {
            const ctx = canvas.getContext("2d");
            const s = window.screen;
            const width = (canvas.width =
                getMatrixElementWidth - (decrease ? decrease : 0));
            const height = (canvas.height = s.height);
            const letters = Array(256)
                .join(1)
                .split("");
            const draw = () => {
                canvas.getContext("2d").fillStyle = "rgba(0,0,0,.05)";
                canvas.getContext("2d").fillRect(0, 0, width, height);
                canvas.getContext("2d").fillStyle = "#0F0";
                letters.map(function(y_pos, index) {
                    text = String.fromCharCode(3e4 + Math.random() * 33);
                    x_pos = index * 10;
                    canvas.getContext("2d").fillText(text, x_pos, y_pos);
                    letters[index] =
                        y_pos > 758 + Math.random() * 1e4 ? 0 : y_pos + 10;
                });
            };
            setInterval(draw, 33);
        } else {
            console.log("Your browser do not support canvas]");
        }
    }
};

setTimeout(() => {
    displayMatrixBackground("matrix-background", "matrix-canvas-header");
    displayMatrixBackground(
        "matrix-background-footer",
        "matrix-canvas-footer",
        20
    );
}, 0);
