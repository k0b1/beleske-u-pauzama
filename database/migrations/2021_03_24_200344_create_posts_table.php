<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('post_title');
            $table->string('post_subtitle')->nullable();
            $table->string('post_logo')->nullable();
            $table->json('post_tags')->nullable();
            $table->string('post_category')->nullable();
            $table->longText('post_content');
            $table->longText('post_draft')->nullable();
            $table->string('post_type')->nullable();
            $table->string('post_author');
            $table->string('post_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
