<?php

namespace App\Http\Traits;

use App\Models\Posts;
use App\Models\Tags;
use Illuminate\Http\Request;

trait TakePostData
{
    public function takePostData(Request $request, Posts $post)
    {
        $post->post_title = $request["post-title"];
        $post->post_subtitle = $request["post-subtitle"];

        $post->post_content = $request["wysiwyg-editor"];
        $post->post_draft = $request["wysiwyg-editor"];

        $post->post_author = $request["post-author"];
        $post->post_slug = $request["post-slug"];
        $post->excerpt = $request["post-excerpt"];
        $post->post_category = $request["post-category"];
        $post->updated_at = $request["post-date"];
        $post->post_tags = json_encode($request["dashboard-multiple-tags"]);

        $post->meta_keywords = $request["meta-keywords"];
        $post->meta_description = $request["meta-description"];

        if (isset($request["update"]) && !empty($request["update"])) {
            $post->post_type = $request["update"];
        }
        if (isset($request["draft"]) && !empty($request["draft"])) {
            $post->post_type = $request["draft"];
        }

        if ($request->hasFile("file")) {
            $post->post_image = $request->file("file")->store("public");
        }
        return $post;
    }

    public function takeTagData(Request $request, Tags $tag)
    {
        $tag->tag_name = $request["tag-name"];
        $tag->tag_description = $request["tag-description"];

        return $tag;
    }
}
