<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Posts;
use App\Models\Tags;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Traits\TakePostData;
use App\Models\Categories;

class DashboardEditPostController extends Controller
{
    use TakePostData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('dashboard.dashboard-edit-post', [
            'post' => Posts::get()->where('id', '=', $id),
            'tags' => Tags::get()->toArray(),
            'categories' => Categories::get()->toArray(),
            'author' => Posts::join("users", "posts.post_author", "=", 'users.id')
                ->select()
                ->where('posts.id', '=', $id)
                ->get(),
            'id' => $id
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Posts::find($id);
        $post = $this->takePostData($request, $post);
        $post->save();

        return view('dashboard', [
            'success' => "Sve je kul",
            // 'post' => Posts::get()->where('id', '=', $id),
            // 'tags' => Tags::get()->toArray(),
            // 'author' => User::join("posts", "posts.post_author","=", 'users.id')
            //               ->select()
            //               ->where('posts.id', '=', $id)
            //               ->get(),
            // 'id' => $id
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Posts::find($id);
        $post->delete();
        return view('dashboard');
    }
}
