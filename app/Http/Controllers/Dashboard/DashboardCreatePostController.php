<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Posts;
use App\Models\Tags;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Traits\TakePostData;
use App\Models\Categories;

class DashboardCreatePostController extends Controller
{
    use TakePostData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('dashboard.dashboard-create-post', [

            'tags' => Tags::get()->toArray(),
            'categories' => Categories::get()->toArray(),
            'user' => User::where('name', '=', 'kobi')->get(),
            'author' => User::get()
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Posts;
        $post = $this->takePostData($request, $post);
        $post->save();
        return view('dashboard');
    }
}
