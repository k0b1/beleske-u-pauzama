<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Traits\TakePostData;
use App\Models\Tags;
use Illuminate\Http\Request;

class DashboardTagsController extends Controller
{
    use TakePostData;
    //
    public function index()
    {
        return view('dashboard.dashboard-list-tags', [
            'tags' => Tags::get()
        ]);
    }

    public function update(Request $request, $id)
    {
        $tag = Tags::find($id);
        if (!empty($request['tag-name'])) {
            $tag->tag_name = $request['tag-name'];
        }
        if (!empty($request['tag-description'])) {
            $tag->tag_description = $request['tag-description'];
        }
        $tag->save();
        return redirect('dashboard-tags');
    }

    public function create(Request $request)
    {
        $tag = new Tags;
        $tag = $this->takeTagData($request, $tag);
        $tag->save();
        return redirect('dashboard-tags');
    }
}
