<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use Illuminate\Http\Request;

class DashboardCategoriesController extends Controller
{
    //
    public function index()
    {
        return view('dashboard.dashboard-list-categories', [
            'categories' => Categories::get()
        ]);
    }

    public function update(Request $request, $id)
    {
        echo $id;
        $category = Categories::find($id);
        if (!empty($request['category-name'])) {
            $category->category_name = $request['category-name'];
        }
        if (!empty($request['category-description'])) {
            $category->category_description = $request['category-description'];
        }
        $category->save();
        return redirect('dashboard-categories');
    }
}
