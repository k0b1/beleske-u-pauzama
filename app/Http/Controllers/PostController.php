<?php

namespace App\Http\Controllers;

use App\Models\Posts;
use App\Models\Tags;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Http\Request;

class PostController extends Controller
{
    use Sluggable;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    // 032 663 688
    //
    // sreda subota 19:00
    //



    /**
     * Display the specified resource.
     *
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    public function show($name)
    {
        //
        return view('post', [
            'post' => Posts::select("posts.*")
                ->where('post_slug', '=', $name)
                ->where('post_type', '=', 'published')
                ->get(),
            "tags" => json_decode(
                Tags::select("tags.*")
                    ->orderBy("tags.id", "DESC")
                    ->get()
            ),
        ]);
    }
    // Posts::join('users', 'users.id', '=', 'posts.post_author')
    //                         ->where('users.id', '=', '13')
    //                         ->select('posts.*')->where('post_slug', '=', $name)
    //                          ->get()
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
