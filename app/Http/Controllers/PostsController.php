<?php

namespace App\Http\Controllers;

use App\Models\Posts;
use App\Models\Tags;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public $limit = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view("welcome", [
            "posts" => Posts::join(
                "categories",
                "categories.id",
                "=",
                "posts.post_category"
            )
                ->orderBy("posts.created_at", "DESC")
                ->select(
                    "posts.*",
                    "categories.*",
                    "posts.created_at",
                    "posts.id"
                )
                ->where('post_type', '=', 'published')
                ->paginate($this->limit),
            "number_of_posts" => json_decode(
                Posts::where('post_type', '=', 'published')->count()
            ),
            "limit" => $this->limit
        ]);
    }

    /**
     * Display the specified resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function show_blog()
    {
        //
        return view("duh-u-okovu", [
            //'posts' => Posts::orderBy('id', 'DESC')->where('post_category', '=', 'Duh u oklopu')->get(),
            "posts" => Posts::join(
                "categories",
                "categories.id",
                "=",
                "posts.post_category"
            )
                ->orderBy("posts.id", "DESC")
                ->select("posts.*")
                ->where("categories.id", "=", "3")
                ->where('posts.post_type', "=", 'published')
                ->paginate($this->limit),
            "tags" => json_decode(
                Tags::select("tags.*")
                    ->orderBy("tags.id", "DESC")
                    ->get()
            ),
            "number_of_posts" => Posts::where("posts.post_category", "=", "3")
                ->where('posts.post_type', "=", 'published')
                ->count(),
            "limit" => $this->limit
        ]);
    }

    /**
     * Display the specified resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function show_reviews()
    {
        //
        return view("osvrti", [
            "posts" => Posts::join(
                "categories",
                "categories.id",
                "=",
                "posts.post_category"
            )
                ->orderBy("posts.created_at", "DESC")
                ->select("posts.*")
                ->where("categories.id", "=", "1")
                ->where('posts.post_type', "=", 'published')
                ->paginate($this->limit),
            "tags" => json_decode(
                Tags::select("tags.*")
                    ->orderBy("tags.id", "DESC")
                    ->get()
            ),
            "number_of_posts" => Posts::where("posts.post_category", "=", "1")
                ->where('posts.post_type', "=", 'published')
                ->count(),
            "limit" => $this->limit
        ]);
    }

    public function show_free_software()
    {
        return view("slobodni-softver", [
            "posts" => Posts::join(
                "categories",
                "categories.id",
                "=",
                "posts.post_category"
            )
                ->orderBy("posts.created_at", "DESC")
                ->select("posts.*")
                ->where("categories.id", "=", "4")
                ->where('posts.post_type', "=", 'published')
                ->paginate($this->limit),
            "tags" => json_decode(
                Tags::select("tags.*")
                    ->orderBy("tags.id", "DESC")
                    ->get()
            ),
            "number_of_posts" => Posts::where("posts.post_category", "=", "4")
                ->where('posts.post_type', "=", 'published')
                ->count(),
            "limit" => $this->limit

        ]);
    }

    public function show_about()
    {
        return view("o-meni", [
            "post" => Posts::join(
                "categories",
                "categories.id",
                "=",
                "posts.post_category"
            )
                ->where("categories.id", "=", "5")
                ->where('posts.post_type', "=", 'published')
                ->select("posts.*")
                ->orderBy("posts.created_at", "DESC")
                ->get(),
            "tags" => json_decode(
                Tags::select("tags.*")
                    ->orderBy("tags.id", "DESC")
                    ->get()
            ),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
