<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    use HasFactory;

    protected $fillable = [
      'tag_name',
      'tag_description'
    ];

    public function getFullAttribute(): string
    {
        return $this->attributes['tag_name'] . ' (' . $this->attributes['tag_description'] . ')';
    }
}
