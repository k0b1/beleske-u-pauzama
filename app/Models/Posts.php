<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    use HasFactory;

    protected $fillable = [
        'post_title',
        'post_subtitle',
        'excerpt',
        'post_logo',
        'post_tags',
        'post_author',
        'post_category',
        'post_content',
        'post_type',
        'post_image',
        'post_slug'
    ];
}
