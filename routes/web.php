<?php

use App\Http\Controllers\Dashboard\DashboardEditPostController;
use App\Http\Controllers\Dashboard\DashboardListPostsController;
use App\Http\Controllers\Dashboard\DashboardCreatePostController;
use App\Http\Controllers\Dashboard\DashboardTagsController;
use App\Http\Controllers\Dashboard\DashboardCategoriesController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\TagsController;
use App\Http\Controllers\CkeditorController;
use App\Http\Controllers\PreviewController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostsController::class, 'show']);

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

Route::post('upload_image', [PostController::class, 'show']);

Route::get('/osvrti',  [PostsController::class, 'show_reviews']);

Route::get('/post/{slug}', [PostController::class, 'show']);

Route::get('/blog', [PostsController::class, 'show_blog']);

Route::get('/bitovi-i-bajtovi', [PostsController::class, 'show_free_software']);

Route::get('/tag/{slug}', [TagsController::class, 'show']);

Route::get('/page/{slug}', [PageController::class, 'show']);

Route::get('/dashboard', function () {
      return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/preview/post/{slug}', [PreviewController::class, 'show'])->middleware(['auth']);

Route::get('/dashboard-posts', [DashboardListPostsController::class, 'show'])->middleware(['auth'])->name('');
Route::get('/dashboard-edit-post/{slug}', [DashboardEditPostController::class, 'show'])->middleware(['auth'])->name('');
Route::post('/dashboard-edit-post/{slug}', [DashboardEditPostController::class, 'update'])->middleware(['auth'])->name('');
Route::delete(
      '/delete-post/{slug}',
      [DashboardEditPostController::class, 'destroy']
)
      ->middleware(['auth']);


Route::get('/dashboard-create-post', [DashboardCreatePostController::class, 'index'])->middleware(['auth'])->name('');
Route::post('/dashboard-create-post', [DashboardCreatePostController::class, 'store'])->middleware(['auth'])->name('');

Route::get('/dashboard-tags', [DashboardTagsController::class, 'index'])->middleware(['auth']);
Route::post('/update-tag/{id}', [DashboardTagsController::class, 'update'])->middleware(['auth']);

Route::get('/create-new-tag', [DashboardTagsController::class, 'create'])->middleware(['auth']);
Route::post('/create-new-tag', [DashboardTagsController::class, 'create'])->middleware(['auth']);

Route::get('/dashboard-categories', [DashboardCategoriesController::class, 'index'])->middleware(['auth']);

Route::get('ck-editor', [CkeditorController::class, 'index'])->middleware(['auth']);
Route::post('ck-editor/imgupload', [CkeditorController::class, 'imgupload'])->middleware(['auth'])->name('ckeditor.upload');

require __DIR__ . '/auth.php';
